import openpyxl
import requests
import json
from openpyxl.styles import Font
from icecream import ic
from os import system, name

"""
Functions needed in the program.
"""

def find_basisjaar(aanvangsjaar):
    match aanvangsjaar:
        case aanvangsjaar if aanvangsjaar >= 2013:
            return "2013 = 100"
        case aanvangsjaar if aanvangsjaar >= 2004:
            return "2004 = 100"
        case aanvangsjaar if aanvangsjaar >= 1996:
            return "1996 = 100"

def find_maand(maand_nr, jaar):
    if maand_nr == 1:
        maand_nr = "12"
        jaar -= 1
    else:
        maand_nr = str(maand_nr - 1)
    return f"{months_json[maand_nr]} {jaar}"

def huidige_maand(maand):
    jaar = maand.split()[1]
    match maand.split()[0]:
        case "Januari":
            return f"Februari {jaar}"
        case "Februari":
            return f"Maart {jaar}"
        case "Maart":
            return f"April{jaar}"
        case "April":
            return f"Mei {jaar}"
        case "Mei":
            return f"Juni {jaar}"
        case "Juni":
            return f"Juli {jaar}"
        case "Juli":
            return f"Augustus {jaar}"
        case "Augustus":
            return f"September {jaar}"
        case "September":
            return f"Oktober {jaar}"
        case "Oktober":
            return f"November {jaar}"
        case "November":
            return f"December {jaar}"
        case "December":
            return f"Januari {jaar + 1}"

"""
Opening and loading the necessary files for the program to work with.
"""

# Set path to JSON files
path_gezondheidsindex_json = "gezondheidsindex.json"
path_months_json = "months.json"

# Open the excel files and loading worksheets
woningen_xlsx = openpyxl.load_workbook(filename="woningen.xlsx")
huurprijs_xlsx = openpyxl.load_workbook(filename="huurprijs.xlsx")

woningen_worksheet = woningen_xlsx["woningen"]

# Loading the JSON files
with open(path_gezondheidsindex_json, "r") as f:
    gezondheidsindex_json = json.load(f)
gezondheidsindex_json_list = gezondheidsindex_json["facts"]

with open(path_months_json, "r") as f:
    months_json = json.load(f)

# Tabellen op StatBEL
# https://bestat.statbel.fgov.be/bestat/login.xhtml

# Getting the index from the last month as a list
url_gindex_api_json_laatste_maand = "https://bestat.statbel.fgov.be/bestat/api/views/cbe6f7e8-1dfa-482d-a80a-be26bc005f18/result/JSON"

gindex_laatste_maand = requests.get(url_gindex_api_json_laatste_maand).json()
gindex_laatste_maand_list = gindex_laatste_maand["facts"]

# Code needs only to be execute when there is a new index month.
list_of_months = []
for x in range(len(gezondheidsindex_json_list)):
    list_of_months.append(gezondheidsindex_json_list[x]["Maand"])

if gindex_laatste_maand_list[-1]["Maand"] not in list_of_months:
    #"""
    #Will append the new indexes if the new month is available.
    #"""
    #for x in gindex_laatste_maand["facts"]:
    #    gezondheidsindex_json["facts"].append(x)
    #with open(path_gezondheidsindex_json, "w") as f:
    #    json.dump(gezondheidsindex_json, f, indent=2)
    """
    Look for new adresses in woningen_xlsx (column basisindex empty),
    find the right index in gezondheidsindex_json_list and write it to that cell
    """
    for rij in woningen_worksheet.iter_rows(min_row=2):
        adres_cel = rij[0]
        basisindex_cel = rij[3]
        aanvangsdatum_cel = rij[2]

        if adres_cel.value is not None and basisindex_cel.value is None:
            aanvangsmaand = find_maand(aanvangsdatum_cel.value.month, aanvangsdatum_cel.value.year)
            basisjaar = find_basisjaar(int(aanvangsmaand.split()[1]))
            #ic(basisjaar)
            #ic(aanvangsmaand)
            for x in range(len(gezondheidsindex_json_list)):
                if gezondheidsindex_json_list[x]["Maand"] == aanvangsmaand and gezondheidsindex_json_list[x]["Basisjaar"] == basisjaar:
                    #ic(gezondheidsindex_json_list[x]["Gezondheidsindex"])
                    basisindex_cel.value = gezondheidsindex_json_list[x]["Gezondheidsindex"]
                    break
            
    """
    Create a new worksheet in huurpijs.xlsx named with 'Month year' of the relevant month the rent is due.
    Then copy the data from moningen.xlsx to that new worksheet
    """
    huurprijs_active_worksheet = huurprijs_xlsx.create_sheet(huidige_maand(gezondheidsindex_json_list[-1]["Maand"]))
    for rij in woningen_worksheet.iter_rows():
        for cel in rij:
            huurprijs_active_worksheet[cel.coordinate].value = cel.value

    woningen_xlsx.save("woningen.xlsx")
    huurprijs_xlsx.save("huurprijs.xlsx")

else:
    ic(False)


